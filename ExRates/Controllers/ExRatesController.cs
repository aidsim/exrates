﻿using System.Collections.Generic;
using System.Web.Http;
using RestSharp;
using ExRates.Models;
using System;
using System.Threading.Tasks;
using System.Linq;
using System.Net.Http;
using System.Net;

namespace ExRates.Controllers
{
    public class ExRatesController : ApiController
    {
        public async Task<List<ExRateChange>> Get(string date)
        {

            Validate(date);

            var task1 = Task.Factory.StartNew(() => GetExRates(date));
            var yesterday = Convert.ToDateTime(date).AddDays(-1).ToString("yyyy-MM-dd");

            var task2 = Task.Factory.StartNew(() => GetExRates(yesterday));

            await Task.WhenAll(task1, task2);

            var exRates = task1.Result;
            var exRatesFromYesterday = task2.Result;

            var result = exRates.Join(exRatesFromYesterday,
                arg => arg.Currency,
                arg => arg.Currency,
                (first, second) =>
                new ExRateChange
                {
                    Date = first.Date,
                    CurrentRate = first.Rate,
                    PreviousRate = second.Rate,
                    Currency = first.Currency,
                    Change = first.Rate - second.Rate,
                    ChangePct = (first.Rate - second.Rate) / second.Rate
                }).ToList();

            return result.OrderByDescending(a => a.Change).ToList();
        }

        private void Validate(string date)
        {
            DateTime dateParsed;
            if (string.IsNullOrEmpty(date))
            {
                HttpResponseMessage msg = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Date parameter was not provided.");
                throw new HttpResponseException(msg);
            }

            if (string.IsNullOrEmpty(date) || !DateTime.TryParse(date, out dateParsed))
            {
                HttpResponseMessage msg = Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Date provided is invalid.");
                throw new HttpResponseException(msg);
            }

        }

        private List<Item> GetExRates(string date)
        {

            var client = new RestClient("http://www.lb.lt/webservices/ExchangeRates/ExchangeRates.asmx");
            var request = new RestRequest("getExchangeRatesByDate", Method.GET);
            request.AddQueryParameter("Date", date);

            var response = client.Execute<ExRatesList>(request);

            if(response.StatusCode != HttpStatusCode.OK)
            {
                HttpResponseMessage msg = Request.CreateErrorResponse(HttpStatusCode.BadGateway, "Unable to get exchange rates.");
                throw new HttpResponseException(msg);
            }

            return response.Data.ExchangeRates;
        }
    }
}
