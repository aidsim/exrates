﻿using System;

namespace ExRates.Models
{
    public class ExRateChange
    {
        public DateTime Date { get; set; }
        public string Currency { get; set; }
        public decimal CurrentRate { get; set; }
        public decimal PreviousRate { get; set; }
        public decimal Change { get; set; }
        public decimal ChangePct { get; set; }
    }
}