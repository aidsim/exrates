﻿using System;
using System.Collections.Generic;

namespace ExRates.Models
{
    public class ExRatesList
    {
        public List<Item> ExchangeRates { get; set; }
    }

    public class Item
    {
        public DateTime Date { get; set; }
        public string Currency { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }
        public string Unit { get; set; }
    }
}