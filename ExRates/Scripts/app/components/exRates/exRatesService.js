﻿(function () {

    var app = angular.module('exRatesApp');

    app.factory('exRatesService', ['$http', '$q', 'ngSettings', function ($http, $q, ngSettings) {

        var baseUrl = ngSettings.apiServiceBaseUri + "ExRates/";

        var getExRates = function (date) {
            return $http.get(baseUrl, {
                params: {
                    date: date
                }
            })
                .then(onComplete, onError);
        };

        var onComplete = function (response) {            
            return response.data;
        };

        var onError = function (error) {
            return $q.reject(error);
        };

        return {
            getExRates: getExRates
        };

    }]);

})();