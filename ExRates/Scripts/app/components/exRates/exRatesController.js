﻿(function () {
    'use strict';

    var momentDateFormat = "YYYY/MM/DD";
    var dateFormat = "yyyy/MM/dd";

    var exRatesController = function ($scope, exRatesService) {
        $scope.message = "";
        $scope.noExRates = false;
        $scope.isFormSubmitted = false;

        $scope.onDateChanged = function () {
            $scope.isFormSubmitted = false;
            $scope.noExRates = false;
            $scope.exRates = [];
        }

        var getExRates = function (form) {
            $scope.message = "";
            $scope.isFormSubmitted = true;
            if (form.$valid) {
                var dateString = moment($scope.dt).format(momentDateFormat);
                exRatesService.getExRates(dateString).then(onGetExRatesComplete, onGetExRatesError);
            }
        }

        var onGetExRatesComplete = function (data) {
            if (data.length == 0) {
                $scope.noExRates = true;
            }
            else {
                $scope.noExRates = false;
                $scope.exRates = data;
            }
        }

        var onGetExRatesError = function (error) {
            $scope.message = error.data.Message;
        }

        $scope.onSearchExRates = getExRates;

        $scope.clear = function () {
            $scope.dt = null;
        };

        $scope.inlineOptions = {
            showWeeks: true
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2014, 11, 31),
            startingDay: 1
        };

        $scope.onDatePickerOpen = function () {
            $scope.datepicker.opened = true;
        };

        $scope.setDate = function (year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        $scope.format = dateFormat;

        $scope.datepicker = {
            opened: false
        };

    };

    var app = angular.module('exRatesApp');
    app.controller('exRatesController', exRatesController);
    exRatesController.$inject = ["$scope", "exRatesService"];

})();
