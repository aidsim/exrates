﻿(function () {
    'use strict';

    var appName = "exRatesApp";

    var app = angular.module(appName, [
        'ngResource',
        'ui.bootstrap',
        'angularValidator'
    ]);

    var apiBaseUri = 'http://localhost:51510/';
    var apiServiceBaseUri = apiBaseUri + 'api/';
    app.constant('ngSettings', {
        apiServiceBaseUri: apiServiceBaseUri,
        apiBaseUri: apiBaseUri
    });
})();